import {generatorID} from "./functions.js";

export function validateInputCategory(obj, categoryName) {
    const [...inputs] = document.querySelectorAll(".category-info input");
    console.log(inputs);
    inputs.forEach((el) => {
        if (el.value.length >= 3) {
            obj.id = generatorID();
            obj.dataAdd =
                `${new Date().getFullYear()}-${new Date().getMonth() + 1}-${new Date().getDate()} ${new Date().getHours()}:${new Date().getMinutes()}:${new Date().getSeconds()}`
            if (el.dataset.type === "Назва продукта") {
                obj.productName = el.value;
            } else if (el.dataset.type === "Введіть вартість") {
                obj.porductPrice = parseFloat(el.value);
            } else if (el.dataset.type === "Посилання на зображення") {
                obj.productImage = el.value
            } else if (el.dataset.type === "Опис товару") {
                obj.productDescription = el.value;
            } else if (el.dataset.type === "Гарячі слова, розділяйте комою.") {
                obj.keywords.push(...el.value.split(","))
            //dop data for restoran
            }else if ((el.dataset.type === "Введіть грамовку")) {
                obj.productWeiht = el.value;
            }else if ((el.dataset.type === "Введіть склад (через коми)")) {    
                obj.ingredients.push(...el.value.split(","));
            }else if ((el.dataset.type === "Вартість страви")) {
                obj.price = el.value;
            }else if ((el.dataset.type === "Зобреження")) {
                obj.productImageUrl = el.value;  
            }else if ((el.dataset.type === "Вага фінальна")) {
                obj.Weiht = el.value;
            //dop data for video hosting
            }else if ((el.dataset.type === "Рік виходу")) {
                obj.premierYear = el.value;
            }else if ((el.dataset.type === "Країна")) {
                obj.country = el.value;
            }else if ((el.dataset.type === "Кіностудія")) {
                obj.studio = el.value;
            }else if ((el.dataset.type === "Посилання на фільм")) {
                obj.videoUrl = el.value;
            }else if ((el.dataset.type === "Обмеження по віку")) {
                obj.ageRestrictions = el.value;
            }else if ((el.dataset.type === "Рейтинг")) {
                obj.like = el.value;
            }
            el.value = "";
            el.classList.remove("error");
        } else {
            el.classList.add("error")
            return
        }
    });
    console.log(categoryName);
    if(categoryName === "Магазин"){
        let data = JSON.parse(localStorage.BDStore);
        data.push(obj)
        localStorage.BDStore = JSON.stringify(data);
    }else if (categoryName === "Ресторан") {
        let data = JSON.parse(localStorage.BDRestoran);
        data.push(obj)
        localStorage.BDRestoran = JSON.stringify(data);
    }else if (categoryName === "Відео хостинг"){ 
        let data = JSON.parse(localStorage.BDVideo);
        data.push(obj)
        localStorage.BDVideo = JSON.stringify(data);
    }else{
        console.error("Поки немає запису");
        return
    }
}
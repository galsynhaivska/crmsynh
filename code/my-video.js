import { editInputProduct, generatorID, closeWindowModal } from "./functions.js";

const data = JSON.parse(localStorage.BDVideo),
    btnSave = document.getElementById("save"),
    btnClose = document.getElementById("close"),
    modalWindow = document.querySelector(".container-modal");
console.log(data);

function showProduct() {
    if (document.location.pathname.search("video") !== -1) {  //
        const tbody = document.querySelector("table tbody");
        tbody.innerHTML = ""
        tbody.insertAdjacentHTML("beforeend", data.map((infoProduct, index) => {
            return `
        
            <tr>
                <td>${index + 1}</td>
                <td>${infoProduct.productName}</td>
                <td title="При настиску сортувати.">${infoProduct.premierYear === "" ? "0" : infoProduct.premierYear}</td>
                <td title="При настиску сортувати.">${infoProduct.videoUrl} </td>
                <td class="edit" data-id="${index}">&#128221;</td>
                <td>${infoProduct.dataAdd ? infoProduct.dataAdd : "Без дати"}</td>
                <td>&#128465;</td>
            </tr>`
        }).join(""))
    }
    const [...edits] = document.querySelectorAll(".edit");
    edits.forEach((tdEdit) => {
        tdEdit.addEventListener("click", (e) => {
            modalWindow.classList.add("active");
            editProduct(data[tdEdit.dataset.id], document.location.pathname)
        })
    })
}

showProduct()

btnClose.addEventListener("click", closeWindowModal);

const editProduct = (product, url = "") => {
    if (url.includes("video")) {
        const { id, keywords, productName, premierYear, country, studio, videoUrl, videoImage, ageRestrictions, like} = product;
        const inputs = [
            editInputProduct("text", keywords, "Ключеві слова", generatorID(), ""),
            editInputProduct("text", productName, "Назва фільму", generatorID(), ""),
            editInputProduct("number", premierYear, "Рік виходу", generatorID(), ""),
            editInputProduct("text",country, "Країна", generatorID(), ""),
            editInputProduct("text", studio, "Кіностудія", generatorID(), ""),
            editInputProduct("text", videoUrl, "Посилання на фільм", generatorID(), ""),
            editInputProduct("text", videoImage, "Зобреження", generatorID(), ""),
            editInputProduct("text", ageRestrictions, "Обмеження по віку", generatorID(), ""),
            editInputProduct("number", like, "Рейтинг", generatorID(), "")
        ]
        document.querySelector(".input-edit").dataset.key = id;
        document.querySelector(".input-edit").innerHTML = "";
        document.querySelector(".input-edit").append(...inputs)
    }

}

function saveBtnClick () {
        const keyForm = document.querySelector(".input-edit").dataset.key;
        const inputs = document.querySelectorAll(".input-edit input");
        const obj = {};
        inputs.forEach((input) => {
            if (input.key === "Ключеві слова") {
                obj.keywords = input.value.split(",");
            } else if (input.key === "Рік виходу") {
                obj.premierYear= parseFloat(input.value);
            } else if (input.key === "Назва фільму") {
                obj.productName = input.value;
            } else if (input.key === "Країна") {
                obj.country = input.value;
            } else if (input.key === "Кіностудія") {
                obj.country = input.value;
            } else if (input.key === "Посилання на фільм") {
                obj.videoUrl = input.value;
            } else if (input.key === "Зобреження") {
                obj.videoImage = input.value;    
            } else if (input.key === "Обмеження по віку") {
                obj.ageRestrictions = input.value;
            }else if (input.key === "Рейтинг") {
                obj.like = input.value;
            }
        })
        const [rez] = data.filter((el, i) => {
            return el.id === keyForm
        })
        obj.id = rez.id;
        obj.data = rez.data;
    
        data.forEach((el, i) => {
            if (el.id === rez.id) {
                data.splice(i, 1, obj)
            }
        })
        localStorage.BDVideo = JSON.stringify(data);
        showProduct();
        closeWindowModal();
}


btnSave.addEventListener("click", saveBtnClick)

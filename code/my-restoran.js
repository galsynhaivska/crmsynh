import { editInputProduct, generatorID, closeWindowModal } from "./functions.js";

const data = JSON.parse(localStorage.BDRestoran),
    btnSave = document.getElementById("save"),
    btnClose = document.getElementById("close"),
    modalWindow = document.querySelector(".container-modal");
console.log(data);

function showProduct() {
    if (document.location.pathname.search("restoran") !== -1) {  //
        const tbody = document.querySelector("table tbody");
        tbody.innerHTML = ""
        tbody.insertAdjacentHTML("beforeend", data.map((infoProduct, index) => {
            return `
            <tr>
                <td>${index + 1}</td>
                <td>${infoProduct.productName}</td>
                <td title="При настиску сортувати.">${infoProduct. Weiht === "" ? "0" : infoProduct. Weiht}</td>
                <td title="При настиску сортувати.">${infoProduct.price} грн.</td>
                <td class="edit" data-id="${index}">&#128221;</td>
                <td>${infoProduct.status ? "&#9989;" : "&#10060;"}</td>
                <td>${infoProduct.dataAdd ? infoProduct.dataAdd : "Без дати"}</td>
                <td>&#128465;</td>
            </tr>`
        }).join(""))
    }
    const [...edits] = document.querySelectorAll(".edit");
    edits.forEach((tdEdit) => {
        tdEdit.addEventListener("click", (e) => {
            modalWindow.classList.add("active");
            editProduct(data[tdEdit.dataset.id], document.location.pathname)
        })
    })
}

showProduct()

btnClose.addEventListener("click", closeWindowModal);

const editProduct = (product, url = "") => {
    if (url.includes("restoran")) {
        const { id, keywords, price, productName,  productWeiht, ingredients, productImageUrl, Weiht, status } = product;
        const inputs = [
            editInputProduct("text", keywords, "Ключеві слова", generatorID(), ""),
            editInputProduct("number", price, "Вартість страви", generatorID(), ""),
            editInputProduct("text", productName, "Назва страви", generatorID(), ""),
            editInputProduct("text", productWeiht, "Грамовка", generatorID(), ""),
            editInputProduct("text", ingredients, "Інгредієнти (розділяйте комою)", generatorID(), ""),
            editInputProduct("text", productImageUrl, "Картинка страви", generatorID(), ""),
            editInputProduct("number", Weiht, "Вага фінальна", generatorID(), ""),
            editInputProduct("checkbox", status, "Наявніть/статус", generatorID(), "")
        ]
        document.querySelector(".input-edit").dataset.key = id;
        document.querySelector(".input-edit").innerHTML = "";
        document.querySelector(".input-edit").append(...inputs)
    }

}


function saveBtnClick () {
        const keyForm = document.querySelector(".input-edit").dataset.key;
        const inputs = document.querySelectorAll(".input-edit input");
        const obj = {};
        inputs.forEach((input) => {
            if (input.key === "Ключеві слова") {
                obj.keywords = input.value.split(",");
            } else if (input.key === "Вартість страви") {
                obj.price = parseFloat(input.value);
            } else if (input.key === "Назва страви") {
                obj.productName = input.value;
            } else if (input.key === "Грамовка") {
                obj.productWeiht = input.value;
            } else if (input.key === "Інгредієнти (розділяйте комою)") {
                obj.ingredients = input.value.split(",");
            } else if (input.key === "Картинка страви") {
                obj.productImageUrl = input.value;
            } else if (input.key === "Вага фінальна") {
                obj.Weiht = parseFloat(input.value);    
            } else if (input.key === "Наявніть/статус") {
                obj.status = input.checked;
            }
        });
        const [rez] = data.filter((el, i) => {
            return el.id === keyForm
        })
        obj.id = rez.id;
        obj.data = rez.data;
    
        data.forEach((el, i) => {
            if (el.id === rez.id) {
                data.splice(i, 1, obj)
            }
        })
        localStorage.BDRestoran = JSON.stringify(data);
        showProduct();
        closeWindowModal();
}


btnSave.addEventListener("click", saveBtnClick)

import { validateInputCategory } from "./my-functions.js";
import {createCategoryInputModal} from "./functions.js";

if (!localStorage.BDStore) {
    localStorage.BDStore = JSON.stringify([])
} else {
    console.log(localStorage.BDStore)
}
//Video hosting
if (!localStorage.BDVideo) {
    localStorage.BDVideo = JSON.stringify([])
} else {
    console.log(localStorage.BDVideo)
}
// Restoran
if (!localStorage.BDRestoran) {
    localStorage.BDRestoran = JSON.stringify([])
} else {
    console.log(localStorage.BDRestoran)
}

let categoryName = null;

const  saveItem = () =>  {
    if(categoryName === "Ресторан") {
        let objInfo = {
            id: "",
            productName: "",
            productWeiht:"",
            ingredients: [],
            price: "",
            productImageUrl: "",
            keywords: [],
            Weiht : "",
        };
        validateInputCategory(objInfo, categoryName);
    }else if (categoryName === "Відео хостинг"){
            let objInfo = {
                id: "",
                productName: "",
                premierYear:"",
                country: "",
                studio:"",
                videoUrl: "",
                videoImage: "",
                keywords: [],
                ageRestrictions : "",
                like : "" ,
            };
            validateInputCategory(objInfo, categoryName);
    }else if (categoryName === "Магазин"){
                let objInfo = {
                    id: "",
                    productName: "",
                    porductPrice: "",
                    productImage: "",
                    productDescription: "",
                    productQuantity: "",
                    keywords: [],
                };
                validateInputCategory(objInfo, categoryName);
    }else{
        console.error("Поки немає реалізації");
        return
    }
};

try{
//main-page with navigate to restoran and video
const btnModal = document.getElementById("btn-modal"),
    modalWindow = document.querySelector(".container-modal"),
    selectModal = document.getElementById("select-data"),
    btnClose = document.getElementById("close"),
    btnSave = document.getElementById("save"),
    categoryInfo = document.querySelector(".category-info");

    btnModal.addEventListener("click", () =>{
        modalWindow.classList.add("active");
    });

    btnClose.addEventListener("click", () => {
        modalWindow.classList.remove("active");
    });

    selectModal.addEventListener("change", (e) => {
        categoryInfo.innerHTML = ""
        if (e.target.value === "Ресторан") {
            categoryName = "Ресторан";
            categoryInfo.insertAdjacentHTML("afterbegin", createCategoryInputModal(restoration).join(""));
        }else if (e.target.value === "Відео хостинг"){
            categoryName = "Відео хостинг";
            categoryInfo.insertAdjacentHTML("afterbegin", createCategoryInputModal(video).join(""));
        } else if (e.target.value === "Магазин") {
            categoryName = "Магазин";
            categoryInfo.insertAdjacentHTML("afterbegin", createCategoryInputModal(store).join(""));
        }else{
            console.error("Поки немає реалізації");
        //    return
        }
    })

    btnSave.addEventListener("click", saveItem);

}catch(er){
    console.error(er)
}



const restoration = ["Назва продукта", "Введіть грамовку", "Введіть склад (через коми)", "Вартість страви", "Зобреження", "Гарячі слова, розділяйте комою.", "Вага фінальна"];
const store = ["Назва продукта", "Введіть вартість", "Посилання на зображення", "Опис товару", "Гарячі слова, розділяйте комою."]
const video = ["Назва продукта", "Рік виходу", "Країна", "Кіностудія", "Посилання на фільм", "Зобреження", "Гарячі слова, розділяйте комою.", "Обмеження по віку", "Рейтинг"];